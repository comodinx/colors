//
// constants
//
const isEnabledOnStringProto = !['false', '0'].includes(String(process.env.COLORS_ON_STRING_PROTO).toLowerCase());

const defaultColors = ['black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white', ['grey', 90, 100]];
const regexpColorStyle = /\x1B\[\d+m/g;
const codePrefix = '\x1b[';
const codeSuffix = 'm';
const closeForeground = `${codePrefix}39${codeSuffix}`;
const closeBackground = `${codePrefix}49${codeSuffix}`;

const colors = { isEnabledOnStringProto };
const foregrounds = {};
const backgrounds = {};
const brightForegrounds = {};
const brightBackgrounds = {};
const specials = {
    reset: [`${codePrefix}0${codeSuffix}`, `${codePrefix}0${codeSuffix}`],
    bold: [`${codePrefix}1${codeSuffix}`, `${codePrefix}22${codeSuffix}`],
    dim: [`${codePrefix}2${codeSuffix}`, `${codePrefix}22${codeSuffix}`],
    italic: [`${codePrefix}3${codeSuffix}`, `${codePrefix}23${codeSuffix}`],
    underline: [`${codePrefix}4${codeSuffix}`, `${codePrefix}24${codeSuffix}`],
    blink: [`${codePrefix}5${codeSuffix}`, `${codePrefix}25${codeSuffix}`],
    inverse: [`${codePrefix}7${codeSuffix}`, `${codePrefix}27${codeSuffix}`],
    hidden: [`${codePrefix}8${codeSuffix}`, `${codePrefix}28${codeSuffix}`],
    strikethrough: [`${codePrefix}9${codeSuffix}`, `${codePrefix}29${codeSuffix}`]
};

// build colors. format, tuple codes [open, close]
defaultColors.forEach((color, i) => {
    const name = Array.isArray(color) ? color[0] : color;
    const code = Array.isArray(color) ? color : [];

    // foreground's
    foregrounds[name] = [`${codePrefix}${i + (code[1] || 30)}${codeSuffix}`, closeForeground];
    brightForegrounds[name] = [`${codePrefix}${i + (code[1] || 90)}${codeSuffix}`, closeForeground];

    // background's
    backgrounds[name] = [`${codePrefix}${i + (code[2] || 40)}${codeSuffix}`, closeBackground];
    brightBackgrounds[name] = [`${codePrefix}${i + (code[2] || 100)}${codeSuffix}`, closeBackground];
});

//
// functions
//

// string extender
const extend = name => {
    String.prototype.__defineGetter__(name, function () {
        return colors[name](this);
    });
};

// styling string
const styling = (prefix, str, suffix) => {
    return `${prefix}${str}${suffix}`;
    // -->   \______/ \___/ \______/
    // -->     open   string  close
};

// builder color functions
const builder = (prefix, codes) => {
    for (let color in codes) {
        const name = `${prefix}${!prefix ? color.charAt(0) : color.charAt(0).toUpperCase()}${color.slice(1)}`;

        // set styling function on colors
        colors[name] = str => styling(codes[color][0], str, codes[color][1]);

        if (isEnabledOnStringProto) {
            // set styling function on string prototype
            extend(name);
        }
    }
};

// Clean colors from string
colors.strip = str => ('' + str).replace(regexpColorStyle, '');

// Alias for colors names
colors.setAlias = aliases => {
    for (let alias in aliases) {
        const name = aliases[alias];

        if (!colors[name]) {
            continue;
        }

        // set alias function on colors
        colors[alias] = colors[name];

        if (isEnabledOnStringProto) {
            // set alias function on string prototype
            extend(alias);
        }
    }
};

// build color functions
builder('', specials);
builder('', foregrounds);
builder('bg', backgrounds);
builder('bright', brightForegrounds);
builder('bgBright', brightBackgrounds);
extend('strip');

//
// exports
//
module.exports = colors;
