# @comodinx/colors

This is a simple proof of concept. An exercise.

We were inspired by and strongly recommend using [colors](https://www.npmjs.com/package/colors)

## Installation

```sh
npm i @comodinx/colors
```

## colors and styles!

### text and bright text colors

| color     | bright          | 
|:----------|:----------------|
| black     |                 |
| red       | brightRed       |
| green     | brightGreen     |
| yellow    | brightYellow    |
| blue      | brightBlue      |
| magenta   | brightMagenta   |
| cyan      | brightCyan      |
| white     | brightWhite     |
| grey      |                 |

### background and bright background colors

| color     | bright          | 
|:----------|:----------------|
| bgBlack   |                 |
| bgRed     | bgBrightRed     |
| bgGreen   | bgBrightGreen   |
| bgYellow  | bgBrightYellow  |
| bgBlue    | bgBrightBlue    |
| bgMagenta | bgBrightMagenta |
| bgCyan    | bgBrightCyan    |
| bgWhite   | bgBrightWhite   |
| bgGrey    |                 |

### styles

| style         |
|:--------------|
| reset         |
| bold          |
| dim           |
| italic        |
| underline     |
| inverse       |
| hidden        |
| strikethrough |

## Usage

The super nifty way

```js
require('@comodinx/colors');

console.log('hello'.green);
console.log('i like cake and pies'.underline.red);
console.log('inverse the color'.inverse);
```

or a slightly less nifty way which doesn't extend String.prototype

```js
const colors = require('@comodinx/colors/safe');

console.log(colors.green('hello'));
console.log(colors.red(colors.underline('i like cake and pies')));
console.log(colors.inverse('inverse the color'));
```

## Prevent extend `String.prototype`

```sh
COLORS_ON_STRING_PROTO=0 node myapp.js
```

## Console.log [string substitution](http://nodejs.org/docs/latest/api/console.html#console_console_log_data)

```js
const name = 'Marak';
console.log(colors.green('Hello %s'), name);
// outputs -> 'Hello Marak'
```

## Alias

```js
const colors = require('@comodinx/colors');

colors.setAlias({
    title: 'bold',
    success: 'green',
    error: 'red',
    warn: 'yellow',
    info: 'cyan'
});

console.log('🟢  Congratulations'.success);
console.log('🔴  ERROR'.error);
console.log('🟡  ¡¡¡WARNING!!!'.warn);
console.log('🔵  This is an information message'.info);
console.log('This is a title for success message'.title.success);
```
