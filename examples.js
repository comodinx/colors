//
// imports
//
const colors = require('./colors');

//
// examples
//

//
// with colors function
//
console.log('\n\n=================================================');
console.log('========== USAGE WITH COLORS FUNCTIONS ==========');
console.log('=================================================\n');
console.log(colors.yellow('First some yellow text'));
console.log(colors.yellow(colors.underline('Underline that text')));
console.log(colors.red(colors.bold('Make it bold and red')));
// styles not widely supported
console.log(colors.bold(colors.italic(colors.underline(colors.red('Chains are also cool.')))));
// styles not widely supported
console.log(colors.green('So ') + colors.underline('are') + ' ' + colors.inverse('inverse') + colors.yellow(colors.bold(' styles! ')));
// remark: .strikethrough may not work with Mac OS Terminal App
console.log('This is ' + colors.strikethrough('not') + ' fun.');
console.log(colors.black(colors.bgWhite('Background color attack!')));
console.log(colors.brightCyan('Blindingly ') + colors.brightRed('bright? ') + colors.brightYellow('Why ') + colors.brightGreen('not?!'));
console.log(colors.red('This\nis\na\nmultiline\nmessage'));
console.log(colors.strip(colors.brightCyan('This ') + colors.brightRed('is ') + colors.brightYellow('a ') + colors.brightGreen('clean message')));

//
// with string prototype
//
console.log('\n\n=================================================');
console.log('========== USAGE WITH STRING PROTOTYPE ==========');
console.log('=================================================\n');

if (!colors.isEnabledOnStringProto) {
    console.log(colors.yellow('⚠️  string prototype color extention is not enabled for safe mode\n\n'));
}
else {
    console.log('First some yellow text'.yellow);
    console.log('Underline that text'.yellow.underline);
    console.log('Make it bold and red'.red.bold);
    // styles not widely supported
    console.log('Chains are also cool.'.bold.italic.underline.red);
    // styles not widely supported
    console.log('So '.green + 'are'.underline + ' ' + 'inverse'.inverse + ' styles! '.yellow.bold);
    // remark: .strikethrough may not work with Mac OS Terminal App
    console.log('This is ' + 'not'.strikethrough + ' fun.');
    console.log('Background color attack!'.black.bgWhite);
    console.log('Blindingly '.brightCyan + 'bright? '.brightRed + 'Why '.brightYellow + 'not?!'.brightGreen);
    console.log('This\nis\na\nmultiline\nmessage'.red);
    console.log(('This '.brightCyan + 'is  '.brightRed + 'a '.brightYellow + 'clean message'.brightGreen).strip);
}

//
// with alias
//
console.log('\n\n=================================================');
console.log('==========      USAGE WITH ALIAS       ==========');
console.log('=================================================\n');

colors.setAlias({
    title: 'bold',
    success: 'green',
    error: 'red',
    warn: 'yellow',
    info: 'cyan'
});

console.log('✅  Congratulations'.success);
console.log('❌  ERROR'.error);
console.log('⚠️  ¡¡¡WARNING!!!'.warn);
console.log('ℹ️  This is an information message'.info);
console.log('This is a title for success message'.title.success);

//
// index
//
console.log('\n\n=================================================');
console.log('==========           INDEX             ==========');
console.log('=================================================\n');

console.log('text colors\n');
console.log('  -', 'black'.black);
console.log('  -', 'red'.red);
console.log('  -', 'green'.green);
console.log('  -', 'yellow'.yellow);
console.log('  -', 'blue'.blue);
console.log('  -', 'magenta'.magenta);
console.log('  -', 'cyan'.cyan);
console.log('  -', 'white'.white);
console.log('  -', 'grey'.grey);

console.log('\nbright text colors\n');
console.log('  -', 'brightRed'.brightRed);
console.log('  -', 'brightGreen'.brightGreen);
console.log('  -', 'brightYellow'.brightYellow);
console.log('  -', 'brightBlue'.brightBlue);
console.log('  -', 'brightMagenta'.brightMagenta);
console.log('  -', 'brightCyan'.brightCyan);
console.log('  -', 'brightWhite'.brightWhite);

console.log('\nbackground colors\n');
console.log('  -', 'bgBlack'.bgBlack);
console.log('  -', 'bgRed'.bgRed);
console.log('  -', 'bgGreen'.bgGreen);
console.log('  -', 'bgYellow'.bgYellow);
console.log('  -', 'bgBlue'.bgBlue);
console.log('  -', 'bgMagenta'.bgMagenta);
console.log('  -', 'bgCyan'.bgCyan);
console.log('  -', 'bgWhite'.bgWhite);
console.log('  -', 'bgGrey'.bgGrey);

console.log('\nbright background colors\n');
console.log('  -', 'bgBrightRed'.bgBrightRed);
console.log('  -', 'bgBrightGreen'.bgBrightGreen);
console.log('  -', 'bgBrightYellow'.bgBrightYellow);
console.log('  -', 'bgBrightBlue'.bgBrightBlue);
console.log('  -', 'bgBrightMagenta'.bgBrightMagenta);
console.log('  -', 'bgBrightCyan'.bgBrightCyan);
console.log('  -', 'bgBrightWhite'.bgBrightWhite);

console.log('\nstyles\n');
console.log('  -', 'reset'.reset);
console.log('  -', 'bold'.bold);
console.log('  -', 'dim'.dim);
console.log('  -', 'italic'.italic);
console.log('  -', 'underline'.underline);
console.log('  -', 'inverse'.inverse);
console.log('  -', 'hidden'.hidden);
console.log('  -', 'strikethrough'.strikethrough);
